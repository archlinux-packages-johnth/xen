# Maintainer: John Thomson <aur.xen at j0aur.mm.st>
# Contributor: David Sutton <kantras - gmail.com>
# Contributor: Shanmu Thiagaraja <sthiagaraja+AUR@prshanmu.com>
# Contributor: Limao Luo
# Contributor: Luceo
# Contributor: Revellion

_build_qemu_traditional="${build_qemu_traditional:-false}"
_build_stubdom="${build_stubdom:-false}"
_system_seabios="${system_seabios:-true}"
_system_ovmf="${system_ovmf:-true}"
_system_ipxe="${system_ipxe:-true}"
_build_debug="${build_debug:-false}"
_build_livepatch="${build_livepatch:-false}"

_system_seabios_path="/usr/share/qemu/bios-256k.bin"
_system_ovmf_path="/usr/share/ovmf/x64/OVMF.fd"
_system_ipxe_path="/usr/lib/ipxe/ipxe.pxe"
#_system_qemu_path="/usr/bin/qemu-system-x86_64"
_build_livepatch=true

# Xen & dependency versions
# SeaBIOS & OVMF tags are in src/xen-*/tools/Config.mk
# grep -rE '_(REVISION|VERSION|TAG)( \?| :){0,1}=' src/xen**/{Config.mk,stubdom/configure,tools/firmware/etherboot/Makefile}
_git_ref_xen='#branch=staging'
_git_ref_qemuu='#branch=master'
_git_ref_qemuu_ui_keycodemapdb='#branch=master'
_git_ref_qemuu_dtc='#branch=master'
_git_ref_qemuu_capstone='#branch=master'
_git_ref_qemuu_tests_fp_berkeley_testfloat_3='#branch=master'
_git_ref_qemuu_tests_fp_berkeley_softfloat_3='#branch=master'
_git_ref_qemuu_slirp='#branch=master'

_git_ref_qemut='#branch=master'
_git_ref_minios='#branch=master'
_git_ref_seabios='#branch=master'
_git_ref_ovmf='#branch=master'
_git_ref_ipxe='1dd56dbd11082fb622c2ed21cfaced4f47d798a6'

pkgbase=xen-git
_pkgname=xen
pkgver=1
_pkgver=${pkgver/rc/-rc}
_pkgname_dist="$_pkgname"
pkgrel=1
pkgdesc='Virtual Machine Hypervisor & Tools'
arch=(x86_64 armv7h)
depends=(
    bridge-utils
    curl
    gnutls
    iproute2
    libaio
    libcacard
    libcap-ng
    libiscsi
    libnl
    libpng
    lzo
    pciutils
    python
    sdl
    systemd
    usbredir
    yajl
)
[[ "$CARCH" == 'x86_64' ]] && depends+=(
    lib32-glibc
)
[[ "$CARCH" == *'arm'* ]] && depends+=(
    dtc
)
optdepends=()
url='http://www.xenproject.org/'
license=('GPL2')
makedepends=(
    cmake
    figlet
    git
    markdown
    nasm
    ocaml-findlib
    spice-protocol
    wget
)
[[ "$CARCH" == 'x86_64' ]] && makedepends+=(
    bin86
    dev86
    iasl
)

options=(!buildflags !strip)
changelog=ChangeLog

source=(
    "git://xenbits.xen.org/xen.git${_git_ref_xen}"

    ## Compile patches

    ## Files
    xen.install
    21_linux_xen_multiboot_arch
    efi-xen.cfg
    "tmpfiles.d-$_pkgname.conf"

    ## XSA patches
)

noextract=()

validpgpkeys=('23E3222C145F4475FA8060A783FE14C957E82BD9')
#gpg --keyserver pgp.mit.edu --recv-key 23E3222C145F4475FA8060A783FE14C957E82BD9
sha256sums=(
    'SKIP'

    #pkgbuild files
    '53b1618d331457fb197abb00ed446c7a6387c34e9236199312b72ccbbaea5703'
    'ed94be4f7b6e472acc747737f7bcc97b9a58db5c24c2ec58b4c776ac3da0fb6e'
    'ceaff798a92a7aef1465a0a0b27b1817aedd2c857332b456aaa6dd78dc72438f'
    '40e0760810a49f925f2ae9f986940b40eba477dc6d3e83a78baaae096513b3cf'

    ## XSA patches
)

_config_custom=()

if [[ -n "$_system_qemu_path" ]]; then
    _config_custom+=("--with-system-qemu=$_system_qemu_path")
    depends+=(
        qemu
    )
else
    depends+=(
        spice
    )
    source+=(
        'qemu-xen'::"git://xenbits.xen.org/qemu-xen.git${_git_ref_qemuu}"
        'qemu-xen-sub-keycodemapdb'::"git://git.qemu-project.org/keycodemapdb.git${_git_ref_qemuu_ui_keycodemapdb}"
        'qemu-xen-sub-capstone'::"git://git.qemu-project.org/capstone.git${_git_ref_qemuu_capstone}"
        'qemu-xen-sub-testfloat'::"git://git.qemu-project.org/capstone.git${_git_ref_qemuu_tests_fp_berkeley_testfloat_3}"
        'qemu-xen-sub-testsoftfloat'::"git://git.qemu-project.org/capstone.git${_git_ref_qemuu_tests_fp_berkeley_softfloat_3}"
        'qemu-xen-sub-slirp'::"git://git.qemu-project.org/capstone.git${_git_ref_qemuu_slirp}"
    )
    sha256sums+=(
        'SKIP'
        'SKIP'
        'SKIP'
        'SKIP'
        'SKIP'
        'SKIP'
    )
    if [[ ! "$CARCH" == *'x86'* ]]; then
        source+=(
            'qemu-xen-sub-dtc'::"git://git.qemu-project.org/dtc.git${_git_ref_qemuu_dtc}"
        )
        sha256sums+=(
            'SKIP'
	)
    fi
    _config_custom+=(--with-extra-qemuu-configure-args='--disable-gtk --enable-spice --enable-usb-redir')
fi


if [[ "$_build_qemu_traditional" == false ]]; then
    _config_custom+=(--disable-qemu-traditional)
    _system_ipxe=false
    # ipxe requires rombios
    # rombios is disabled when qemut disabled
else
    source+=(
        'qemu-xen-traditional'::"git://xenbits.xen.org/qemu-xen-traditional.git${_git_ref_qemut}"
    )
    sha256sums+=(
        'SKIP'
    )
fi

if [[ "$_build_stubdom" == true ]]; then
    if [[ "$CARCH" == *'arm'* ]]; then
        echo '####Compile settings error:'
        echo "#cannot build stubdom for $CARCH"
        _build_stubdom=false
    else
        echo '#building with stubdom'

        xen_stubdom_files=(
            http://xenbits.xen.org/xen-extfiles/lwip-1.3.0.tar.gz
            http://xenbits.xen.org/xen-extfiles/zlib-1.2.3.tar.gz
            http://xenbits.xen.org/xen-extfiles/newlib-1.16.0.tar.gz
            http://xenbits.xen.org/xen-extfiles/pciutils-2.2.9.tar.bz2
            http://xenbits.xen.org/xen-extfiles/polarssl-1.1.4-gpl.tgz
            http://xenbits.xen.org/xen-extfiles/grub-0.97.tar.gz
            http://xenbits.xen.org/xen-extfiles/tpm_emulator-0.7.4.tar.gz
            http://xenbits.xen.org/xen-extfiles/gmp-4.3.2.tar.bz2
            http://caml.inria.fr/pub/distrib/ocaml-4.02/ocaml-4.02.0.tar.gz
        )

        source+=(
            'mini-os'::"git://xenbits.xen.org/mini-os.git${_git_ref_minios}"
            "${xen_stubdom_files[@]}"
        )

        sha256sums+=(
            #stubdom bits
            'SKIP'
            '772e4d550e07826665ed0528c071dd5404ef7dbe1825a38c8adbc2a00bca948f'
            '1795c7d067a43174113fdf03447532f373e1c6c57c08d61d9e4e9be5e244b05e'
            'db426394965c48c1d29023e1cc6d965ea6b9a9035d8a849be2750ca4659a3d07'
            'f60ae61cfbd5da1d849d0beaa21f593c38dac9359f0b3ddc612f447408265b24'
            '2d29fd04a0d0ba29dae6bd29fb418944c08d3916665dcca74afb297ef37584b6'
            '4e1d15d12dbd3e9208111d6b806ad5a9857ca8850c47877d36575b904559260b'
            '4e48ea0d83dd9441cc1af04ab18cd6c961b9fa54d5cbf2c2feee038988dea459'
            '936162c0312886c21581002b79932829aa048cfaf9937c6265aeaa14f1cd1775'
            'dbbcbd72a29a51206677a606ea09dfec83ae25cbbf52dee90306bc04812cd034'

            #stubdom patches
        )

        for filename in "${xen_stubdom_files[@]}"; do
            noextract+="${filename##*/}"
        done

        _config_custom+=(--enable-stubdom)

        # Options to build individual stubdoms
        _config_custom+=(
            #--enable-ioemu-stubdom=no
            #--enable-c-stubdom=no
            #--enable-caml-stubdom=no
            #--enable-pv-grub=no
            #--enable-xenstore-stubdom=no
            #--enable-vtpm-stubdom=no
            #--enable-vtpmmgr-stubdom=no
        )
    fi
else
    _config_custom+=(--disable-stubdom)
fi

if [[ "$_system_seabios" == true ]]; then
    depends+=(
        seabios
    )
    _config_custom+=(--with-system-seabios="${_system_seabios_path}")
else
    source+=(
        'seabios'::"git://xenbits.xen.org/seabios.git${_git_ref_seabios}"
    )
    sha256sums+=(
        'SKIP'
    )
fi

if [[ "$_system_ovmf" == true ]]; then
    optdepends+=(
        ovmf
    )
    _config_custom+=(--with-system-ovmf="${_system_ovmf_path}")
else
    source+=(
        'ovmf'::"git://xenbits.xen.org/ovmf.git${_git_ref_ovmf}"
    )
    sha256sums+=(
        'SKIP'
    )
    _config_custom+=(--enable-ovmf)
fi

if [[ "$_system_ipxe" == true ]]; then
    depends+=(
        #ipxe-git
    )
    _config_custom+=(--with-system-ipxe="${_system_ipxe_path}")
else
    if [[ ! "$_build_qemu_traditional" == false ]]; then
    # ipxe requires rombios, which is enabled by qemut
        source+=(
            "http://xenbits.xen.org/xen-extfiles/ipxe-git-${_git_ref_ipxe}.tar.gz"
        )
        sha256sums+=(
            'fcb2b5da90a59a0011db7403a1ea7887b0dfb04ef91a7d31964c63ed14f7a426'
        )
        noextract=(
            "ipxe-git-${_git_ref_ipxe}.tar.gz"
        )
    fi
fi

pkgver() {
  cd "$_pkgname"
  printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

_makevars=(
    LANG=C
)

prepare() {
    _XEN_SOURCE_DIR="${srcdir}/$_pkgname_dist"
    cd "$_XEN_SOURCE_DIR"

    _xen_dependency_repos=(
        OVMF_UPSTREAM_URL="$srcdir/ovmf"
        #QEMU_UPSTREAM_URL="$srcdir/qemu-xen"
        #QEMU_TRADITIONAL_UPSTREAM_URL="$srcdir/qemu-xen-traditional"
        SEABIOS_UPSTREAM_URL="$srcdir/seabios"
        MINIOS_UPSTREAM_URL="$srcdir/mini-os"
    )

    for repo in "${_xen_dependency_repos[@]}"; do
        eval "export $repo"
        repo_name="${repo%=*}"
        eval echo "# $repo_name = \$$repo_name"
    done

    if [[ ! -n "$_system_qemu_path" ]]; then
        # For QEMU dependencies: Xen will not checkout appropriate revision
        # if QEMU_{UPSTREAM,TRADITIONAL}_URL are accessible directories.
        # make -C tools qemu-...-force-update will not work without configure
        # Do it manually here.
        git clone "$srcdir/qemu-xen" tools/qemu-xen-dir-remote
        cd tools/qemu-xen-dir-remote
        git checkout $(grep -R 'QEMU_UPSTREAM_REVISION.*=' \
                      "${_XEN_SOURCE_DIR}/Config.mk" \
                      | tail -n 1 | sed 's/.*= \(.*\)/\1/')
        git submodule init
        git config submodule.ui/keycodemapdb.url "$srcdir/qemu-xen-sub-keycodemapdb"
        git config submodule.dtc.url "$srcdir/qemu-xen-sub-dtc"
        git config submodule.capstone.url "$srcdir/qemu-xen-sub-capstone"
        git config submodule.dtc.url "$srcdir/qemu-xen-sub-testfloat"
        git config submodule.dtc.url "$srcdir/qemu-xen-sub-testsoftfloat"
        git config submodule.dtc.url "$srcdir/qemu-xen-sub-slirp"
    fi

    if [[ ! "$_build_qemu_traditional" == false ]]; then
        cd "$_XEN_SOURCE_DIR"
        git clone "$srcdir/qemu-xen-traditional" tools/qemu-xen-traditional-dir-remote
        cd tools/qemu-xen-traditional-dir-remote
        git checkout $(grep -R 'QEMU_TRADITIONAL_REVISION.*=' \
                      "${_XEN_SOURCE_DIR}/Config.mk" \
                      | tail -n 1 | sed 's/.*= \(.*\)/\1/')
    fi
    cd "$_XEN_SOURCE_DIR"

    ### Patching

    # XSA Patches
    echo 'XSA patches'
    # Security Patches - Base

    # Security Patches - qemu-xen-traditional
    #cd 'tools/qemu-xen-traditional/'
    #cd '../../'

    # Security Patches - qemu-xen (upstream)
    #cd 'tools/qemu-xen/'
    #cd '../../'


    # Compile Patches
    echo 'Compile patches'

    #cd tools/qemu-xen-dir-remote
    cd "$_XEN_SOURCE_DIR"

    # OVMF Compile support (Pulls from GIT repo, so patching to patch after pull request)
    #mkdir -p tools/firmware/ovmf-patches

    ## Fix systemd-modules-load.d/xen.conf
    ## remove nonexistent modules
    find tools -iname 'configure*' -exec sed -i -E -e '
        /^LINUX_BACKEND_MODULES="$/,/^"$/ {
            #Address range where this variable is set
            /"/b;               #Do noting if the line contains "
            /^xen-/!d;          #Delete if does not start with xen
            s/scsibk/scsiback/; #Change scsibk to scsiback
        };' {} \;

    if [[ "$_build_stubdom" == true ]]; then

        for filename in "${xen_stubdom_files[@]}"; do
            ln -sf "$srcdir/${filename##*/}" stubdom/
        done

        ## Stubdom patches
        #cd 'extras/mini-os'
        #cd '../../'

        #vtpm
    fi

    #etherboot
    if [[ ! "$_system_ipxe" == true ]]; then
        ln -sf "$srcdir/ipxe-git-${_git_ref_ipxe}.tar.gz" tools/firmware/etherboot/ipxe.tar.gz
        #cp "$srcdir"/patch-inbuild-ipxe*.patch tools/firmware/etherboot/patches/
    fi

    if [[ "$_build_debug" == true ]]; then
        _config_custom+=('--enable-debug')
        _makevars+=('debug=y')
        echo >>xen/.config CONFIG_DEBUG=y
    fi

    if [[ "$_build_livepatch" == true ]]; then
        echo >>xen/.config CONFIG_LIVEPATCH=y
    fi
}

build() {
    export PATH="${srcdir}/bin:$PATH"
    cd "$_pkgname_dist/"
    ./autogen.sh
    if [[ -a xen/.config ]]; then
        make -C xen "${_makevars[@]}" olddefconfig V=1
    fi
    ./configure --prefix=/usr --sbindir=/usr/bin --with-sysconfig-leaf-dir=conf.d --with-rundir=/run \
        --enable-systemd \
        "${_config_custom[@]}"
    make "${_makevars[@]}" dist
    if [[ "$_build_livepatch" == true ]]; then
        make "${_makevars[@]}" build-tests
    fi
}

_package() {
    _makevars_package=("${_makevars[@]}" DESTDIR="$pkgdir")
    optdepends=(
        'xen-docs: Official Xen documentation'
        'openvswitch: Optional advanced networking support'
        'urlgrabber: Required for xenpvnetboot'
    )
    conflicts=(xen-{git,rc,igvtg,4.{8,9}} xenstore)
    provides=(xenstore)
    replaces=(xen-{git,rc,4.{8,9}})
    backup=(
        etc/conf.d/xen{domains,commons}
        "etc/$_pkgname/grub.conf"
        "etc/$_pkgname/oxenstored.conf"
        "etc/$_pkgname/xl.conf"
    )
    install="$_pkgname.install"

    cd "$_pkgname_dist/"

    make "${_makevars_package[@]}" install-xen
    make "${_makevars_package[@]}" install-tools
    if [[ "$_build_stubdom" == true ]]; then
        make "${_makevars_package[@]}" install-stubdom
    fi
    if [[ "$_build_livepatch" == true ]]; then
        make "${_makevars_package[@]}" install-tests
    fi

    cd "$pkgdir"

    # Install files from AUR package
    install -Dm644 "$srcdir/tmpfiles.d-$_pkgname.conf" "usr/lib/tmpfiles.d/$_pkgname.conf"
    install -Dm755 "$srcdir/21_linux_xen_multiboot_arch" etc/grub.d/21_linux_xen_multiboot_arch
    install -Dm644 "$srcdir/efi-xen.cfg" etc/xen/efi-xen.cfg

    mkdir -p var/log/xen/console

    # Sanitize library path (if lib64 exists)
    if [[ -d usr/lib64 ]]; then
        cd usr/
        mv lib64/* lib/
        rmdir lib64
        cd ../
    fi

    # If EFI binaries built, move to /boot
    if [[ -f usr/lib/efi/xen.efi ]]; then
        mv usr/lib/efi/*.efi boot/
        rmdir usr/lib/efi
    fi

    # Remove syms
    find usr/lib/debug -type f \( -name '*-syms*' -or -name '*\.map' \) -delete
    rmdir --ignore-fail-on-non-empty usr/lib/debug

    # Remove hypervisor boot symlinks
    find boot -type l -delete

    # Documentation cleanup ( see xen-docs package )
    #rm -rf usr/share/doc
    #rm -rf usr/share/man

    # Remove tempdirs
    rmdir run/xen{,stored}
    rmdir run

    # Remove unnecessary qemu ELF support files
    # qemuu
    rm -f usr/share/qemu-xen/qemu/{palcode,openbios,s390}-*
    rm -f usr/share/qemu-xen/qemu/u-boot.e500
    # qemut
    if [[ "$CARCH" == *'x86'* ]]; then
        rm -f usr/share/xen/qemu/openbios-*
    fi

    # adhere to Static Library Packaging Guidelines
    rm -rf usr/lib/*.a

    # Remove unneeded init.d files
    rm -rf etc/init.d
}

_package-docs(){
    _makevars_package=("${_makevars[@]}" DESTDIR="$pkgdir")
    pkgdesc='Xen virtual machine hypervisor documentation'
    arch=('any')
    depends=()
    cd "$_pkgname_dist/"
    make "${_makevars_package[@]}" install-docs
}

_package-syms(){
    _makevars_package=("${_makevars[@]}" DESTDIR="$pkgdir")
    pkgdesc='Xen virtual machine hypervisor debugging symbols'
    arch=('any')
    depends=()
    _installdir="${pkgdir}/usr/lib/debug"
    cd "$_pkgname_dist/"
    install -d -m0755 "$_installdir"
    for _path in $(find xen -type f \( -name '*-syms' -or -name '*\.map' \)); do
        _file=$(basename "$_path")
        _installfile=$(echo "$_file" |
                       sed "s/\([^.]*\)\(\.*\)/\1-${_pkgver}\2/" )
        install -D -m0644 -p "$_path" "$_installdir/$_installfile"
    done
}

# from linux PKGBUILD, allow pkgbase / pkgname changes
pkgname=("${pkgbase}"{,-docs,-syms})
for _p_name in "${pkgname[@]}"; do
    # create needed PKGBUILD package_pkgname function
    # echo _package-suffix function
    # run _package-suffix function
    eval "package_${_p_name}() {
        $(declare -f "_package${_p_name#${pkgbase}}")
        _package${_p_name#${pkgbase}}
    }"
done
